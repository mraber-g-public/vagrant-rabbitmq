# Vagrant : RabbitMQ

Création et configuration d'une VM Linux Centos 7.1 avec RabbitMQ 3.6.0 à l'aide de Ansible

## Accès

* Port 5672 : RabbitMQ
    - vhost : "/bus"
    - users :
        + `mqadmin` / `mqadmin` (administrator)
        + `rabbit` / `abcd1234`
* Port 15672 : IHM administration RabbitMQ

## commandes

    # Démarrage VM
    $> vagrant up
    # Arrêt VM
    $> vagrant halt
    # Connexion à la VM
    $> vagrant ssh
    # Reconfiguration
    $> vagrant reload --provision
    # Suppression
    $> vagrant destroy -f

## Remarques ##

* Vagrant 1.8.1 : bug dans la gestion de `ansible_local` sous Windows (#6757)
    - Corrigé dans la v1.8.2
    - Contournement détaillé dans l'ano sur Github (patch script Ruby Vagrant)
